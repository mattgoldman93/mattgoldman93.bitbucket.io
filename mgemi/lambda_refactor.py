import boto3
import os
table = boto3.resource('dynamodb').Table('mgemi')

def build_response(message, message_type="Close", sessionAttributes=None, intentName=None, slotToElicit=None, slots={}):
    
    resp = {
        "dialogAction": {
            "type": message_type,
            "message": {
                "contentType": "PlainText",
                "content": message
            }
        }
    }

    if message_type is 'Close':
        resp["dialogAction"]["fulfillmentState"] = "Fulfilled"
    elif message_type is 'ElicitSlot':
        resp["dialogAction"]["intentName"] = intentName
        resp["dialogAction"]["slots"] = slots
        resp["dialogAction"]["slotToElicit"] = slotToElicit
    if sessionAttributes:
        resp['sessionAttributes'] = sessionAttributes
    return resp

def lambda_handler(event, context):
    print(event)
    # check that FindShoeIntent is what's being invoke
    if 'FindShoeIntent' == event['currentIntent']['name']:
       # set variables
        intent_name = event['currentIntent']['name']
        color = None 
        size = None
        shoe_type = None
        audience = None 
        # if input exists, append input transcript, else create list and append
        if 'transcript' in event['sessionAttributes']:
            event['sessionAttributes']['transcript'] += " U: {}".format(event['inputTranscript'])
        else:
            event['sessionAttributes']['transcript'] = "S: Ciao! Welcome to M. Gemi! Let's find you the perfect pair of shoes. Today, are you looking for women's or men's shoes?"
            event['sessionAttributes']['transcript'] += " U: {}".format(event['inputTranscript'])   

        print(event['sessionAttributes'])
        # assign variables values if they exist
        if event['currentIntent']['slots']['audience']:
            audience = event['currentIntent']['slots']['audience']
        if event['currentIntent']['slots']['size']:
            size = event['currentIntent']['slots']['size']
        if event['currentIntent']['slots']['color']:
            color = event['currentIntent']['slots']['color']
        if event['currentIntent']['slots']['type']:
            shoe_type = event['currentIntent']['slots']['type']
      
         #if all 4 exist return final response
        if size and color and shoe_type and audience:
            event['sessionAttributes']['transcript'] += " S: So, you want size {} {} in {}. Let me find some options that work for you.".format( size, shoe_type, color)
            item = {
                'user_id': event['userId'],
                'intent': intent_name,
                'audience': audience,
                'color': color,
                'size': size,
                'type': shoe_type,
                'input': event['sessionAttributes']['transcript']
            }
            table.put_item(Item=item)
            return build_response("So, you want size {} {} in {}. Let me find some options that work for you.".format(size, shoe_type, color), message_type="Close", sessionAttributes=event['sessionAttributes'])   
        elif audience is None: 
            event['sessionAttributes']['transcript'] += " S: today are you looking for women's or men's shoes?"
            return build_response("today are you looking for women's or men's shoes?", message_type="ElicitSlot", sessionAttributes=event['sessionAttributes'], intentName = intent_name, slotToElicit="audience", slots={
                "audience": audience,
                "size": size,
                "type": shoe_type,
                "color": color
            })
        elif size is None:
            event['sessionAttributes']['transcript'] += " S: What size shoe are you looking for?"          
            return build_response("What size shoe are you looking for?", message_type="ElicitSlot", sessionAttributes=event["sessionAttributes"], intentName = intent_name, slotToElicit="size", slots={
                "audience": audience,
                "size": size,
                "type": shoe_type,    
                "color": color
            })
        elif shoe_type is None:
            event['sessionAttributes']['transcript'] += " S: What type of shoe would you like?"
            return build_response("What type of shoe would you like?", message_type="ElicitSlot", sessionAttributes=event["sessionAttributes"], intentName = intent_name, slotToElicit="type", slots={
                "audience": audience,
                "size": size,
                "type": shoe_type,    
                "color": color
            })
        elif color is None:
            event['sessionAttributes']['transcript'] += " S: What color shoe are you looking for?"
            return build_response("What color shoe are you looking for?", message_type="ElicitSlot", sessionAttributes=event['sessionAttributes'], intentName = intent_name, slotToElicit="color", slots={
                "audience": audience,
                "size": size,
                "type": shoe_type,
                "color": color
            })
    else:
        return "error: intent name not found"