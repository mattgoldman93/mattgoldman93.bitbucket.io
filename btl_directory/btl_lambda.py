import boto3
from boto3.dynamodb.conditions import Key

businessTable = boto3.resource('dynamodb').Table('belizean_businesses')
humanTable = boto3.resource('dynamodb').Table('customer_names')
dataTable = boto3.resource('dynamodb').Table('btl_data_collection')


# function to build object to send back to Lex
def build_response(message, message_type="Close", sessionAttributes=None, intentName=None, slotToElicit=None, slots={}):
    resp = {
        "dialogAction": {
            "type": message_type,
            "message": {
                "contentType": "PlainText",
                "content": message
            }
        }
    }
    if message_type is 'Close':
        resp["dialogAction"]["fulfillmentState"] = "Fulfilled"
    elif message_type is 'ElicitSlot':
        resp["dialogAction"]["intentName"] = intentName
        resp["dialogAction"]["slots"] = slots
        resp["dialogAction"]["slotToElicit"] = slotToElicit
    if sessionAttributes:
        resp['sessionAttributes'] = sessionAttributes

    return resp


def scan_table(table, filter_key=None, filter_value=None):

    # Perform a scan operation on table.

    if filter_key and filter_value:
        filtering_exp = Key(filter_key).eq(filter_value)
        response = table.scan(FilterExpression=filtering_exp)
    else:
        response = table.scan()
    try:
        print("response['Items']: ", response["Items"])
        print("response count is",
              response["Count"], "\nscanned count is", response["ScannedCount"])
    except:
        print("item returned in unexpected format")
    return response

# handler function

# to do
    # transcript functionality // testing
    # capture intent, slots, phone num if available and user id
    # figure out why two goldman returns no resultz // testing
def lambda_handler(event, context):
    print("handler reached. event is ", event)
    if 'transcript' in event['sessionAttributes']:
        event['sessionAttributes']['transcript'] += " U: {}".format(event['inputTranscript'])
    else:
        event['sessionAttributes']['transcript'] = " S: Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for. First, are you looking for the phone number of a business or an individual?"
        event['sessionAttributes']['transcript'] += " U: {}".format(event['inputTranscript']) 
    # create variables to hold slots within scope of function
    UserType = None
    BusinessName = None
    HumanName = None
    intent_name = None
    # assign variables values if they exist
    if event['currentIntent']['name']:
        intent_name = event['currentIntent']['name']
        item = {
            "user_id": event['userId'],
            "intent": intent_name 
        }
        if 'search_intent' == intent_name:
            if event['currentIntent']['slots']['UserType']:
                UserType = event['currentIntent']['slots']['UserType']
            if event['currentIntent']['slots']['BusinessName']:
                BusinessName = event['currentIntent']['slots']['BusinessName']
            if event['currentIntent']['slots']['HumanName']:
                HumanName = event['currentIntent']['slots']['HumanName']
            #check first if HumanName is filled
            if HumanName:
                name = HumanName.lower().split(' ')
                fn = name[0]
                # if only 1 name is given scan the table for first name
                if len(name) == 1:
                    table_item = scan_table(
                        humanTable, filter_key="first_name", filter_value=fn)
                    # if dict returned has a count of zero scan fn[0] as last name
                    if table_item['Count'] == 0:
                        table_item = scan_table(
                            humanTable, filter_key="last_name", filter_value=fn)
                        # if nothing is found, return message
                        if table_item['Count'] == 0:
                            msg = "Unfortunately, I didn't find a record for {}. Is there anything else I can help you with?".format(HumanName)
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                            return build_response(msg, message_type="ElicitIntent")
                        # if one record is found, return it
                        elif table_item['Count'] == 1:
                            result = table_item['Items'][0]
                            msg = "I found 1 result for {} as {} {} with phone number {}. Again, that is {}. Is there anything else I can help you with?".format(HumanName, result['first_name'], result['last_name'], result['phone_number'], result['phone_number'])
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                        # if multiple are found, return them all
                        else:
                            msg = 'I found {} records for {}. '.format(
                                table_item['Count'], HumanName)
                            num = 0
                            for i in table_item['Items']:
                                num += 1
                                msg += "Record {} is {} {} with phone number {}".format(
                                    num, i['first_name'], i['last_name'], i['phone_number'])
                            msg += "Is there anything else I can help you with?"
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                    # if one result is found for fn scan, return it
                    elif table_item['Count'] == 1:
                        msg = "I found 1 result for {} as {} {} with phone number {}. Again, that is {}. Is there anything else I can help you with?".format(HumanName, result['first_name'], result['last_name'], result['phone_number'], result['phone_number'])
                        event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                        return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                    # if multiple are found, return them
                    else:
                        msg = 'I found {} records for {}. '.format(
                            table_item['Count'], HumanName)
                        num = 0
                        for i in table_item['Items']:
                            num += 1
                            msg += "Record {} is {} {} with phone number {}".format(
                                num, i['first_name'], i['last_name'], i['phone_number'])
                        msg += "Is there anything else I can help you with?"
                        event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                        return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                if len(name) == 2:
                    fn = name[0]
                    ln = name[1]
                    print ("fn, ln", fn, ln)
                    table_item = scan_table(humanTable, filter_key="last_name", filter_value=ln)
                    print("table item count", table_item)
                    if table_item['Count'] == 0:
                        msg = "Unfortunately, I didn't find a record for {}. Is there anything else I can help you with?".format(HumanName)
                        event['sessionAttributes']['transcript'] += " S: {}".format(msg)                    
                        return build_response(msg, message_type="ElicitIntent")
                    elif table_item['Count'] == 1:
                        result = table_item['Items'][0]
                        if fn in result['first_name']:
                            msg = "The phone number for {} is {}. Again, that is {}. Is there anything else I can help you with?".format(HumanName, result['phone_number'], result['phone_number'])
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)                        
                            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                        else:
                            msg = "I could not find an exact match for {} but I did find {} {} with phone number {}. Again, that is {}. Is there anything else I can help you with?".format(HumanName, result['first_name'], result['last_name'], result['phone_number'], result['phone_number'])
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                    else:
                        resnum = 0
                        results = []
                        for i in table_item['Items']: 
                            if fn in i['first_name']:
                                resnum += 1
                                results.append(i)
                        if resnum == 0:
                            msg = "Unfortunately, I didn't find a record for {}. Is there anything else I can help you with?".format(HumanName)
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                            return build_response(msg, message_type="ElicitIntent")
                        elif resnum == 1:
                            result = results[0]
                            msg = "The phone number for {} is {}. Again, that is {}. Is there anything else I can help you with?".format(HumanName, result['phone_number'], result['phone_number'])
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                        else:
                            msg = 'I found {} records for {}. '.format(resnum, HumanName)
                            num = 0
                            for result in results:
                                num += 1
                                msg += "Record {} is {} {} with phone number {}. ".format(num, result['first_name'], result['last_name'], result['phone_number'])
                            msg += "Is there anything else I can help you with?"
                            event['sessionAttributes']['transcript'] += " S: {}".format(msg)                            
                            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                else:
                    msg = "There was a problem. I heard {}. Can you please provide the first name and last name?".format(HumanName)
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)     
                    return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
            elif BusinessName:
                print("businessName is ", BusinessName)
                # if BusinessName slot is filled try to find it in the database
                # assign table_scan variable to result of function scan_table where the table to scan is belizean_businesses and where name equals the value of BusinessName
                table_scan = scan_table(
                    businessTable, filter_key="name", filter_value=BusinessName.lower())
                # if this works, check the length of the 'Items' list in the response. if it is 1 return a response with that records phone number
                if len(table_scan['Items']) == 1:
                    table_item = table_scan['Items'][0]
                    msg = "The phone number for {} is {}. Again that is {}. Is there anything else I can help you with?".format(BusinessName, table_item['phone_number'], table_item['phone_number'])
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)     
                    return build_response(msg, message_type="ElicitIntent")
                # otherwise, iterate through the Items and report phone numbers
                elif len(table_scan['Items']) > 1:
                    msg = "I found {} records for {}. ".format(table_scan['Count'], BusinessName)
                    num = 0
                    for i in table_scan['Items']:
                        num += 1
                        msg += " record {} is {}. The phone number is {}.".format(num, BusinessName, i['phone_number'])
                    msg += " Is there anything else I can help you with?"
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)   
                    return build_response(msg, message_type="ElicitIntent")
            # if something goes wrong build a response informing user that a record couldn't be found
                else:
                    msg = "Unfortunately, I didn't find a record for {}. Is there anything else I can help you with?".format(BusinessName)
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                    return build_response(msg, message_type="ElicitIntent")
            # if BusinessName is not specified, ask for the name of the business
            elif UserType:
                if 'business' == event['currentIntent']['slots']['UserType']:
                    msg = "Got it. What's the name of the business you're looking for?"
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                    return build_response(msg, message_type="ElicitSlot", sessionAttributes=None, intentName=intent_name, slotToElicit="BusinessName", slots={
                        "UserType": UserType,
                        "BusinessName": BusinessName,
                        "HumanName": HumanName
                    })
                else:
                    msg = "Got it. Can you provide the first and the last name of the person you're looking for?"
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                    return build_response(msg, message_type="ElicitSlot", sessionAttributes=None, intentName=intent_name, slotToElicit="HumanName", slots={
                        "UserType": UserType,
                        "BusinessName": BusinessName,
                        "HumanName": HumanName
                    })
            # if no slots are present, there is something wrong with the event sent to this function 
            else:
                print("an error occurred. Event did not include necessary fields: ['currentIntent']['name'], 'currentIntent']['slots']['BusinessName'], or ['currentIntent']['slots']['UserType']")
                if 'count' in event['sessionAttributes']:
                    count = event['sessionAttributes']['count']
                    count = int(count)
                    if count < 3:
                        count += 1
                        event['sessionAttributes']['count'] = count
                        msg = "I'm sorry, I still could not understand your request. Please tell me, are you looking for a business or an individual?"
                        event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                        return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
                    else:
                        msg = "Let me transfer you to an Agent who will be able to assist you"
                        event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                        item['transcript'] = event['sessionAttributes']['transcript']
                        dataTable.put_item(Item=item)
                        return build_response(msg, message_type="Close")
                else:
                    event['sessionAttributes']['count'] = 1
                    msg = "Please tell me, are you looking for a business or an individual?"
                    event['sessionAttributes']['transcript'] += " S: {}".format(msg)
                    return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])

        elif 'HelloIntent' == intent_name:
            msg = "Welcome to BTL's automated directory search. I'm here to help you find the phone numbers you are looking for. First, are you looking for the phone number of a business or an individual?"
            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
        elif 'YepIntent' == intent_name:
            msg = "So, you need more help. I can connect you with an agent or help you find the phone number for a business or a person."
            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
            return build_response(msg, message_type="ElicitIntent", sessionAttributes=event['sessionAttributes'])
        elif 'NopeIntent' == intent_name:
            msg = 'Thank you for stopping by. Have a nice day!'
            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
            item['transcript'] = event['sessionAttributes']['transcript']
            dataTable.put_item(Item=item)
            return build_response(msg, message_type="Close", sessionAttributes=event['sessionAttributes']) 
        elif 'Transfer' == intent_name:
            msg = "Let me transfer you to an Agent who will be able to assist you"
            event['sessionAttributes']['transcript'] += " S: {}".format(msg)
            item['transcript'] = event['sessionAttributes']['transcript']
            dataTable.put_item(Item=item)
            return build_response(msg, message_type="Close")
        else:
            msg = "Can you please tell me if you are looking for a person or a business?"
            return build_response(msg, message_type="Close")
    else:
        return "error: no intent name in event"