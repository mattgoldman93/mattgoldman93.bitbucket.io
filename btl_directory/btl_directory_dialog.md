Directory Assistance

BASIC FLOW:
- Assistant asks if the user is looking for a business or an individual
- At that point, user is prompted to provide either first and last name or name of the business 
	If no record, tell the user there is no record under that name 
	If only one record, provide the number
	Up to three records, tell user the locations and ask them to pick
	If more then three records, tell user there are multiple matches for that record and ask user to provide location (loop with previous)

## Advanced feature: Ask user to provide phone number to send records as a text message?
## Advanced feature: if no record, attempt fuzzy matching (i didn't find a record for X, but I found X1. Is X1 what you're looking for?"

###Businesses
##One record 

## test text dialogue 1
* GeneralInteraction	HelloIntent	hello
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	business #not sure about annotation yet
    - utter_ask_business_name	"Got it. What's the name of the business you're looking for?"
* DirectoryAssistance	ContentOnlyIntent	{ali graphic|BusinessName}
    - utter_provide_phone_number	"The phone number for ali graphic is 276-2344. Again that is 276-2344 "
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no thank you
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

##>3 records
## test text dialogue 2
* GeneralInteraction	HelloIntent	hello
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	looking for a store #not sure about annotation yet
    - utter_ask_business_name	"Got it. What's the name of the business?"
* DirectoryAssistance	ContentOnlyIntent	{777 grocery store|BusinessName}
    - utter_ask_location	"I found multiple records for 777 grocery store. Can you please provide the location of the business you're looking for?" #more than three matches
* DirectoryAssistance	ContentOnlyIntent	in {san pedro|LocationName}
    - utter_provide_phone_number	"Thank you for that information. The phone number for the 777 grocery store in san pedro is 276-2366. Again that is 276-2366"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no thanks that's it
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

##>2 or 3 records
## test text dialogue 3
* GeneralInteraction	HelloIntent	hello
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	looking for a store #not sure about annotation yet
    - utter_ask_business_name	"Got it. What's the name of the business?"
* DirectoryAssistance	ContentOnlyIntent	{maria hair salon|BusinessName}
    - utter_ask_location	"I found more than one records for maria hair salon. Are you looking for the one in belize city or in san ignacio #less than three matches
* DirectoryAssistance	ContentOnlyIntent	{san ignacio|LocationName} please
    - utter_provide_phone_number	"Thank you. The phone number for maria hair salon in san ignacio is 277-6777. That's is 277-6777"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	RepeatIntent	can you repeat please
    - utter_provide_phone_number	"The phone number for maria hair salon in san ignacio is 277-6777. Again that is 277-6777"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no that's it bye
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

## 0 Records
## test text dialogue 4
* GeneralInteraction	HelloIntent	hi
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	i want the phone number of a store#not sure about annotation yet
    - utter_ask_business_name	"I can help you with that. What's the name of the business?"
* DirectoryAssistance	ContentOnlyIntent	{mariana hair|BusinessName}
    - utter_ask_location	"Unfortunately, I didn't find a record for mariana hair."
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no that's it
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"


###Individuals
##One record 

## test text dialogue 5
* GeneralInteraction	HelloIntent	hello
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	looking for a person #not sure about annotation yet
    - utter_ask_business_name	"Got it. Can you provide the first and the last name of the person you're looking for?"
* DirectoryAssistance	ContentOnlyIntent	{ali gonzalez|UserName}
    - utter_provide_phone_number	"The phone number for ali gonzales is 276-2347. Again that is 276-2347"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no thank you
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

##>3 records
## test text dialogue 6
* GeneralInteraction	HelloIntent	hello
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	looking for my friend's phone number #not sure about annotation yet
    - utter_ask_business_name	"Got it. What's the first and the last name ?"
* DirectoryAssistance	ContentOnlyIntent	{maria del mar|UserName} #not sure about annotation
    - utter_ask_location	"I found multiple enties for maria del mar . Can you please provide the address or the city?" #more than three matches
* DirectoryAssistance	ContentOnlyIntent	she lives in {san pedro|LocationName}
    - utter_provide_phone_number	"Thank you for that information. The phone number for maria del mar in san pedro is 276-2368. Again that is 276-2368"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no thanks
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

##>2 or 3 records
## test text dialogue 7
* GeneralInteraction	HelloIntent	hello
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	looking for a person please #not sure about annotation yet
    - utter_ask_business_name	"Got it. What's the first and last name?"
* DirectoryAssistance	ContentOnlyIntent	{pedro bolivar|UserName}
    - utter_ask_location	"I found more than one records for pedro bolivar. Are you looking for the one in belize city or in belmopan #less than three matches
* DirectoryAssistance	ContentOnlyIntent	{belize|LocationName}
    - utter_provide_phone_number	"Thank you. The phone number for pedro bolivar in belize city is 275-6777. That is 275-6777"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	RepeatIntent	can you repeat please
    - utter_provide_phone_number	"The phone number for pedro bolivar in belize city is 275-6777. Again, that is 275-6777"
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no that's it bye
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

## 0 Records
## test text dialogue 8
* GeneralInteraction	HelloIntent	hi
    - utter_welcome	"Welcome to BTL's automated directory search. I'm here to assist you find the phone numbers you are looking for"
    - utter_ask_record_type	"First, are you looking for the phone number of a business or an individual?"
* DirectoryAssistance	SearchIntent	i want the phone number of an individual person#not sure about annotation yet
    - utter_ask_business_name	"I can help you with that. What's the name of the business?"
* DirectoryAssistance	ContentOnlyIntent	{marcela feliz del belize|UserName}
    - utter_ask_location	"Unfortunately, I didn't find a record for marcela feliz del belize."
    - utter_ask_anything_else	"Is there anything else I can help you with?"
* GeneralInteraction	NoIntent	no that's it
    - utter_goodbye	"Thank you for stopping by! Have a wonderful rest of your day"

