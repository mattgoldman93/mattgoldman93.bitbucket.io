import boto3
import time


client = boto3.client('transcribe')

def lambda_handler(event, context):
    print(event)
    file_uri = "https://s3.amazonaws.com/btl-directory-recordings/{}".format(event['Records'][0]['s3']['object']['key'])
    key = file_uri.split('/')
    name = key[-1].replace('+', '')
    name = name.replace('%', '')

    
    print('file_uri is ', file_uri)
    response = client.start_transcription_job(
    TranscriptionJobName='transcript-{}'.format(name),
    LanguageCode='en-US',
    MediaSampleRateHertz=8000,
    MediaFormat='wav',
    Media={
        'MediaFileUri': file_uri
    },
    Settings={
        'ShowSpeakerLabels': True,
        'MaxSpeakerLabels': 2
    })
    print("!!! response is ", response)
    return response
   
   