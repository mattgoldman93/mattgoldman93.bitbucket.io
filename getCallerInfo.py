import boto3

def lambda_handler(event, context):
    print("event is ", event)
    table =   boto3.resource('dynamodb').Table(event['table'])
    user_id = event['userId']
    table_item = table.get_item(TableName=event['table'], Key={'user_id':user_id})
    print("table item is ", table_item)
    return table_item